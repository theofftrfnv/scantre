from tkinter import messagebox, \
    Label, Tk, Menu, Button, \
    scrolledtext, END, \
    INSERT, Scale, Toplevel
from tkinter.ttk import Notebook, Frame, Combobox, Style
import tkinter.filedialog as fd
import cv2
import easyocr
import pytesseract
import os
from PIL import Image, ImageTk

# Глобальные переменные
file_path = ""
dir_path = ""
thresh = ""
list_img = []
# 0 - path, 1 - dir
imp_path_or_dir = 0
count_ru = 1
count_en = 1
count_ru_en = 1
custom_config = r'--oem 3 --psm 6'
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


# Метод распознования текста
def text_recognition(l, txt, btn_export_result, cmb_sc):
    global file_path, imp_path_or_dir, list_img, custom_config

    if imp_path_or_dir == 1:
        txt.delete(1.0, END)

    # easyOCR
    if cmb_sc.get() == "easyOCR":
        if imp_path_or_dir == 0:
            if l == "Русский":
                reader = easyocr.Reader(["ru"])
                result = reader.readtext(file_path, detail=0, paragraph=True)
                txt.insert(INSERT, result)
            else:
                if l == "Английский":
                    reader = easyocr.Reader(["en"])
                    result = reader.readtext(file_path, detail=0, paragraph=True)
                    txt.insert(INSERT, result)
                else:
                    reader = easyocr.Reader(["en", "ru"])
                    result = reader.readtext(file_path, detail=0, paragraph=True)
                    txt.insert(INSERT, result)
        else:
            if l == "Русский":
                for img in list_img:
                    reader = easyocr.Reader(["ru"])
                    result = reader.readtext(img, detail=0, paragraph=True)
                    txt.insert(INSERT, result)
                    txt.insert(INSERT, " ")
            else:
                if l == "Английский":
                    for img in list_img:
                        reader = easyocr.Reader(["en"])
                        result = reader.readtext(img, detail=0, paragraph=True)
                        txt.insert(INSERT, result)
                        txt.insert(INSERT, " ")
                else:
                    for img in list_img:
                        reader = easyocr.Reader(["ru", "en"])
                        result = reader.readtext(img, detail=0, paragraph=True)
                        txt.insert(INSERT, result)
                        txt.insert(INSERT, " ")

    # tesseract
    else:
        if imp_path_or_dir == 0:
            if l == "Русский":
                result = pytesseract.image_to_string(file_path, lang='rus', config=custom_config)
                txt.insert(INSERT, result)
            else:
                if l == "Английский":
                    result = pytesseract.image_to_string(file_path, lang='eng', config=custom_config)
                    txt.insert(INSERT, result)
                else:
                    messagebox.showwarning("Предупреждение", "tesseract не может сканировать "
                                                             "анг. и рус. языки одновременно")
        else:
            if l == "Русский":
                for img in list_img:
                    result = pytesseract.image_to_string(img, lang='rus', config=custom_config)
                    txt.insert(INSERT, result)
                    txt.insert(INSERT, " ")
            else:
                if l == "Английский":
                    for img in list_img:
                        result = pytesseract.image_to_string(img, lang='eng', config=custom_config)
                        txt.insert(INSERT, result)
                        txt.insert(INSERT, " ")
                else:
                    messagebox.showwarning("Предупреждение", "tesseract не может сканировать "
                                                             "анг. и рус. языки одновременно")

    messagebox.showinfo("Успешно", "Сканирование завершено")
    btn_export_result['state'] = 'normal'


# Метод импорта изображения для одиночного сканирования
def import_img(s, btn_scan_file, tab2):
    global file_path, dir_path, list_img, imp_path_or_dir
    imp_path_or_dir = s
    if s == 0:
        filetypes = (("Изображение", "*.jpg *.png *.jpeg *.bmp"),)
        file_path = fd.askopenfilename(title="Открыть файл", initialdir="/",
                                       filetypes=filetypes)
        if file_path == "":
            messagebox.showwarning("Предупреждение", "Вы не выбрали изображение")
        else:
            img = Image.open(file_path)
            img2 = img.resize((879, 685))
            img3 = ImageTk.PhotoImage(img2)
            img_lbl = Label(tab2, image=img3, width=879, height=685)
            img_lbl.image = img3
            img_lbl.grid(column=1, row=0, rowspan=5, padx=(25, 0))

            messagebox.showinfo("Успешно", "Изображение успешно импортировано")
            btn_scan_file['state'] = 'normal'
    else:
        dir_path = fd.askdirectory(title="Открыть папку", initialdir="/")
        if dir_path == "":
            messagebox.showwarning("Предупреждение", "Вы не выбрали директорию")
        else:
            for root, subdirs, files in os.walk(dir_path):
                for file in files:
                    if os.path.splitext(file)[1].lower() in ('.jpg', '.jpeg', '.png', '.bmp'):
                        list_img.append(os.path.join(root, file))

            messagebox.showinfo("Успешно", "Директория успешно импортирована")
            btn_scan_file['state'] = 'normal'


# Запись изображения в файл с видеокамеры для дальнейшего сканирования
def save_img(l, txt, btn_export_result, cmb_sc):
    global thresh, count_ru, count_en, count_ru_en, imp_path_or_dir, file_path
    imp_path_or_dir = 0
    if l == "Русский":
        cv2.imwrite('thresh_ru' + str(count_ru) + '.jpg', thresh)
        file_path = 'thresh_ru' + str(count_ru) + '.jpg'
        count_ru += 1
    else:
        if l == "Английский":
            cv2.imwrite('thresh_en' + str(count_en) + '.jpg', thresh)
            file_path = 'thresh_en' + str(count_en) + '.jpg'
            count_en += 1
        else:
            cv2.imwrite('thresh_ru_en' + str(count_ru_en) + '.jpg', thresh)
            file_path = 'thresh_ru_en' + str(count_ru_en) + '.jpg'
            count_ru_en += 1

    text_recognition(l, txt, btn_export_result, cmb_sc)


# Запись отсканированного текста в текстовый файл
def exp_res(txt):
    # dir_exp = fd.askdirectory(title="Экспорт", initialdir="/")
    files = [("Текстовый файл", "*.txt"), ]
    dir_exp = fd.asksaveasfile(filetypes=files, defaultextension=files)
    result = txt.get("1.0", END)
    f = open(dir_exp.name, 'w')
    f.write(result)
    f.close()
    messagebox.showinfo("Успешно", "Текст успешно сохранен в " + dir_exp.name)


# Вывод окна 'О программе'
def about_app(window):
    new_window = Toplevel(window)
    new_window.title("О программе...")
    new_window.geometry('500x250')
    new_window.iconbitmap('icon.ico')
    new_window.resizable(0, 0)

    lbl = Label(new_window, text="scanTre", font=("SF Pro Display", 15), anchor="w", width=50)
    lbl.grid(column=0, row=0, pady=(5, 50), padx=5)
    lbl = Label(new_window, text="Версия программы: 0.0.3", font=("SF Pro Display", 15), width=50,
                anchor="w")
    lbl.grid(column=0, row=1, pady=5, padx=5)

    lbl = Label(new_window, text="Автор: Студент 4 курса, Трифонов Роман", font=("SF Pro Display", 15), width=50,
                anchor="w")
    lbl.grid(column=0, row=2, pady=5, padx=5)

    lbl = Label(new_window, text="Год выпуска: 2022", font=("SF Pro Display", 15), width=50,
                anchor="w")
    lbl.grid(column=0, row=3, pady=5, padx=5)


def new_theme(name_theme, window, background_label, background_label2, background_label3):
    print("Смена темы")
    if name_theme == "Светлая":
        window.tk.call("set_theme", "light")
        img = ImageTk.PhotoImage(file="background-white.jpg")
        background_label.config(image=img)
        background_label2.config(image=img)
        background_label3.config(image=img)
    else:
        window.tk.call("set_theme", "dark")
        img = ImageTk.PhotoImage(file="background.jpg")
        background_label.config(image=img)
        background_label2.config(image=img)
        background_label3.config(image=img)


# Смена темы приложения (светлая/тёмная)
def theme_app(window, background_label, background_label2, background_label3):
    new_window = Toplevel(window)
    new_window.resizable(0, 0)

    lbl = Label(new_window, text="Выбор темы приложения: ",
                font=("SF Pro Display", 15), width=25,
                anchor="w",)
    lbl.grid(column=0, row=1, pady=(0, 10), padx=5)
    cmb_sc = Combobox(new_window, state="readonly", width=33)
    cmb_sc['values'] = ("Тёмная", "Светлая")
    cmb_sc.current(0)  # Вариант по умолчанию
    cmb_sc.grid(column=0, row=2, pady=(0, 10), padx=5)
    cmb_sc.bind("<FocusIn>", defocus)
    cmb_sc.bind('<<ComboboxSelected>>', lambda _: new_theme(name_theme=cmb_sc.get(),
                                                            window=window,
                                                            background_label=background_label,
                                                            background_label2=background_label2,
                                                            background_label3=background_label3))


# Обнуление всех результатов и состояний кнопок для начала новой задачи
def new_task(txt, btn_scan_file, btn_export_result, tab2, txt_cam):
    img = Image.open("background_img.jpg")
    img2 = ImageTk.PhotoImage(img)
    txt.delete(1.0, END)
    txt_cam.delete(1.0, END)
    img_lbl = Label(tab2, image=img2, width=879, height=685)
    img_lbl.image = img2
    img_lbl.grid(column=1, row=0, rowspan=5, padx=(25, 0))
    btn_scan_file['state'] = 'disabled'
    btn_export_result['state'] = 'disabled'


# Проверка активных портов видеокамер
def list_ports():
    non_working_ports = []
    dev_port = 0
    working_ports = []
    available_ports = []
    while len(non_working_ports) < 6:  # остановка, если больше 5 не рабочих портов подряд
        camera = cv2.VideoCapture(dev_port)
        if not camera.isOpened():
            non_working_ports.append(dev_port)
            # print("Порт %s не работает." %dev_port)
        else:
            is_reading, img = camera.read()
            # w = camera.get(3)
            # h = camera.get(4)
            if is_reading:
                # print("Порт %s работает с разрешением (%s x %s)" %(dev_port,h,w))
                working_ports.append(dev_port)
            else:
                # print("Порт %s с разрешением ( %s x %s) подключен, но не выводит результат" %(dev_port,h,w))
                available_ports.append(dev_port)
        dev_port += 1
    return working_ports


# Обработка и вывод изображения видеокамеры
def show_cam(cap, lb_cam, thr_1, thr_2, cmb_fr, tab_control):
    global thresh
    _, frame = cap.read()

    if tab_control.tabs().index(tab_control.select()) == 2:
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        thresh = cv2.threshold(gray, thr_1.get(), thr_2.get(), cv2.THRESH_BINARY)[1]

        cont = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cont = cont[0] if len(cont) == 2 else cont[1]
        cont = sorted(cont, key=cv2.contourArea, reverse=True)

        draw_cont = cv2.drawContours(frame, cont, -1, (0, 255, 0), 2)

        if cmb_fr.get() == "RGB":
            img = Image.fromarray(rgb)
            img_tk = ImageTk.PhotoImage(image=img)
            lb_cam.img_tk = img_tk
            lb_cam.configure(image=img_tk)
        else:
            if cmb_fr.get() == "Gray":
                img = Image.fromarray(gray)
                img_tk = ImageTk.PhotoImage(image=img)
                lb_cam.img_tk = img_tk
                lb_cam.configure(image=img_tk)
            else:
                if cmb_fr.get() == "Threshold":
                    img = Image.fromarray(thresh)
                    img_tk = ImageTk.PhotoImage(image=img)
                    lb_cam.img_tk = img_tk
                    lb_cam.configure(image=img_tk)
                else:
                    img = Image.fromarray(draw_cont)
                    img_tk = ImageTk.PhotoImage(image=img)
                    lb_cam.img_tk = img_tk
                    lb_cam.configure(image=img_tk)

    lb_cam.after(10, lambda: show_cam(cap, lb_cam, thr_1, thr_2, cmb_fr, tab_control))


# Удаление Анг. и Рус. при выборе tesseract
def change_lan_in_cmb(cmb_sc, cmb_lan):
    if cmb_sc.get() == "tesseract":
        cmb_lan['values'] = ("Русский", "Английский")
    else:
        cmb_lan['values'] = ("Русский", "Английский", "Русский и английский")


def defocus(event):
    event.widget.master.focus_set()


def main():
    # Создание главного окна приложения
    window = Tk()
    window.title("scanTre")
    window.geometry('1280x720')
    window.resizable(0, 0)
    window.iconbitmap('icon.ico')

    # Инициализация полоски меню
    menu = Menu(window)

    # Установка кастомной темы
    s = Style()
    window.tk.call("source", "azure.tcl")
    window.tk.call("set_theme", "dark")

    # Создание вкладок (Главная/Скан иозбражений/Скан с видеокамеры)
    tab_control = Notebook(window)
    tab1 = Frame(tab_control)
    tab2 = Frame(tab_control)
    tab3 = Frame(tab_control)
    tab_control.add(tab1, text='Главная')
    tab_control.add(tab2, text='Скан изображений')
    tab_control.add(tab3, text='Скан с видеокамеры')
    tab_control.pack(expand=1, fill='both')

    background_image = ImageTk.PhotoImage(file="background.jpg")
    background_label = Label(tab1, image=background_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    background_label2 = Label(tab2, image=background_image)
    background_label2.place(x=0, y=0, relwidth=1, relheight=1)
    background_label3 = Label(tab3, image=background_image)
    background_label3.place(x=0, y=0, relwidth=1, relheight=1)

    # 1 - Настройка элементов главного окна
    lbl = Label(tab1, text="Настройки", font=("SF Pro Display", 20), anchor="w", background="#1d1d1d")
    lbl.grid(column=0, row=0, pady=(5, 50), padx=5)

    # 1.1 - Выбор технологии сканирования текста
    lbl = Label(tab1, text="Технология сканирования: ", font=("SF Pro Display", 15), width=25,
                anchor="w", background="#1d1d1d")
    lbl.grid(column=0, row=1, pady=(0, 10), padx=5)
    cmb_sc = Combobox(tab1, state="readonly", width=33)
    cmb_sc['values'] = ("easyOCR", "tesseract")
    cmb_sc.current(0)  # Вариант по умолчанию
    cmb_sc.grid(column=0, row=2, pady=(0, 10), padx=5)
    cmb_sc.bind("<FocusIn>", defocus)

    # 1.2 - Язык сканируемого документа
    lbl = Label(tab1, text="Язык документа: ", font=("SF Pro Display", 15), width=25,
                anchor="w", background="#1d1d1d")
    lbl.grid(column=0, row=3, pady=5, padx=5)
    cmb_lan = Combobox(tab1, state="readonly", width=33)
    cmb_lan['values'] = ("Русский", "Английский", "Русский и английский")
    cmb_lan.current(0)  # Вариант по умолчанию
    cmb_lan.grid(column=0, row=4, pady=(0, 10), padx=5)
    cmb_lan.bind("<FocusIn>", defocus)

    # 1.3 - Выбор видеокамеры
    lbl = Label(tab1, text="Подключенные видеокамеры: ", font=("SF Pro Display", 15), width=25,
                anchor="w", background="#1d1d1d")
    lbl.grid(column=0, row=5, pady=5, padx=5)
    cmb_cam = Combobox(tab1, state="readonly", width=33)
    cmb_cam['values'] = (list_ports())
    cmb_cam.current(0)  # Вариант по умолчанию
    cmb_cam.grid(column=0, row=6, pady=(0, 10), padx=5)
    cmb_cam.bind("<FocusIn>", defocus)

    # 2 - Настройка элементов окна сканирования изображений
    img = Image.open("background_img.jpg")
    img2 = ImageTk.PhotoImage(img)
    img_lbl = Label(tab2, image=img2, width=879, height=685)
    img_lbl.grid(column=1, row=0, rowspan=5, padx=(25, 0))

    # 2.1 - Поле для вывода результата сканирования
    txt = scrolledtext.ScrolledText(tab2, width=25, height=15, font=("SF Pro Display", 15))
    txt.grid(column=0, row=4, pady=5, padx=5)

    # 2.2 - Выбор изображения (скан одной картинки)
    btn_imp_file = Button(tab2, font=("SF Pro Display", 15), width=30, text="Выбрать изображение",
                          command=lambda: import_img(0, btn_scan_file, tab2))
    btn_imp_file.grid(column=0, row=0, pady=5, padx=5)

    # 2.3 - Выбор директории с изображениями (множественное сканирование)
    btn_imp_dir = Button(tab2, font=("SF Pro Display", 15), width=30, text="Выбрать папку с изображениями",
                         command=lambda: import_img(1, btn_scan_file, tab2))
    btn_imp_dir.grid(column=0, row=1, pady=5, padx=5)

    # 2.4 - Запуск сканирования изображения
    btn_scan_file = Button(tab2, font=("SF Pro Display", 15), width=30, text="Сканировать изображение",
                           state='disabled', command=lambda: text_recognition(l=cmb_lan.get(),
                                                                              txt=txt,
                                                                              btn_export_result=btn_export_result,
                                                                              cmb_sc=cmb_sc))
    btn_scan_file.grid(column=0, row=2, pady=5, padx=5)

    # 2.5 - Экспорт отсканированного текста
    btn_export_result = Button(tab2, font=("SF Pro Display", 15), width=30, text="Экспортировать текст",
                               state='disabled', command=lambda: exp_res(txt))
    btn_export_result.grid(column=0, row=3, pady=5, padx=5)

    # 3 - Настройка элементов окна сканирования при помощи видеокамеры
    lbl = Label(tab3, text="Параметр threshold: ", font=("SF Pro Display", 15), width=25,
                background="#1d1d1d", anchor="w")
    lbl.grid(column=0, row=0, pady=5, padx=5)
    lbl = Label(tab3, text="Параметр threshold2: ", font=("SF Pro Display", 15), width=25,
                background="#1d1d1d", anchor="w")
    lbl.grid(column=0, row=2, pady=5, padx=5)
    lbl = Label(tab3, text="Вид обработки камеры: ", font=("SF Pro Display", 15), width=25,
                background="#1d1d1d", anchor="w")
    lbl.grid(column=0, row=4, pady=5, padx=5)

    # 3.1 - Scale для изменения параметров Threshold
    thr_1 = Scale(tab3, font=("SF Pro Display", 10), orient="horizontal",
                  resolution=1, from_=0, to=255, length=300, background="#1d1d1d")
    thr_1.set(125)
    thr_1.grid(column=0, row=1, pady=(0, 10), padx=5)
    thr_2 = Scale(tab3, font=("SF Pro Display", 10), orient="horizontal",
                  resolution=1, from_=0, to=255, length=300, background="#1d1d1d")
    thr_2.set(255)
    thr_2.grid(column=0, row=3, pady=(0, 10), padx=5)

    # 3.2 - Выбор выводимого на экран потока видеокамеры (фильтры)
    cmb_fr = Combobox(tab3, state="readonly", width=33)
    cmb_fr['values'] = ("RGB", "Gray", "Threshold", "Contours")
    cmb_fr.current(0)  # Вариант по умолчанию
    cmb_fr.grid(column=0, row=5, pady=(0, 10), padx=5)
    cmb_fr.bind("<FocusIn>", defocus)

    # 3.3 - Поле для отсканированного текста
    txt_cam = scrolledtext.ScrolledText(tab3, width=25, height=10, font=("SF Pro Display", 15))
    txt_cam.grid(column=0, row=8, pady=5, padx=5)

    # 3.4 - Кнопки запуска экспорта и сканирования
    btn_export_result_cam = Button(tab3, font=("SF Pro Display", 15), width=30,
                                   text="Экспортировать текст", state='disabled',
                                   command=lambda: exp_res(txt_cam))
    btn_export_result_cam.grid(column=0, row=7, pady=5, padx=5)

    btn_scan_file_cam = Button(tab3, font=("SF Pro Display", 15), width=30,
                               text="Сканировать изображение",
                               command=lambda: save_img(l=cmb_lan.get(),
                                                        txt=txt_cam,
                                                        btn_export_result=btn_export_result_cam, cmb_sc=cmb_sc))
    btn_scan_file_cam.grid(column=0, row=6, pady=5, padx=5)

    # Подключение и настройка видеокамеры
    width, height = 879, 685
    cap = cv2.VideoCapture(int(cmb_cam.get()))
    cap.set(cv2.CAP_PROP_FPS, 24)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    lb_cam = Label(tab3, width=879, height=685)
    lb_cam.grid(column=1, row=0, rowspan=20, padx=(27, 0), pady=(0, 55))

    show_cam(cap, lb_cam, thr_1, thr_2, cmb_fr, tab_control)

    # Вкладка Файл (Новая задача/Выход)
    new_item = Menu(menu, tearoff=0)
    new_item.add_command(label='Новая задача', command=lambda: new_task(txt, btn_scan_file,
                                                                        btn_export_result, tab2, txt_cam))
    new_item.add_separator()
    new_item.add_command(label='Выход', command=lambda: exit())
    menu.add_cascade(label='Файл', menu=new_item)

    # Вкладка Справка (О программе...)
    new_item = Menu(menu, tearoff=0)
    new_item.add_command(label='О программе...', command=lambda: about_app(window))
    menu.add_cascade(label='Справка', menu=new_item)

    # Вкладка Настройки (Смена темы)
    # new_item = Menu(menu, tearoff=0)
    # new_item.add_command(label='Смена темы', command=lambda: theme_app(window,
    #                                                                    background_label,
    #                                                                    background_label2,
    #                                                                    background_label3))
    # menu.add_cascade(label='Настройки', menu=new_item)
    window.config(menu=menu)

    window.mainloop()


if __name__ == "__main__":
    main()
